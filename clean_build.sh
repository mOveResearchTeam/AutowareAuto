#!/bin/bash

if [ -z "$1" ]
  then
    export VCS_FILE='autoware.auto.foxy.repos'
elif [ "$1" == 'ssh' ]
  then
    export VCS_FILE='autoware.auto.foxy.ssh.repos'
fi

echo 'Using VCS file: ' $VCS_FILE

echo 'Deleting log build install folders...'
rm -rf log/ build/ install/

echo 'Deleting external folder...'
rm -rf src/external

echo 'Cloning external...'
vcs --worker=1 import < $VCS_FILE

echo 'Rebuilding workspace...'
colcon build --packages-up-to iac_launch --cmake-args -DCMAKE_BUILD_TYPE=Release

echo 'Clean re-build complete. Remember to restart autoware and zennoh services.'
