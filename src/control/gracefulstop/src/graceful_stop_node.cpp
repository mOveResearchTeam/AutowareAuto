#include <graceful_stop_node.hpp>

namespace control 
{

GracefulStop::GracefulStop() : Node("graceful_stop_node")
{
    this->subStopCmd_ = this->create_subscription<std_msgs::msg::Bool>(
        "/vehicle/stop_command", 1, std::bind(&GracefulStop::receiveStopCmd, this, std::placeholders::_1));
    this->subBestgnsspos_ = this->create_subscription<novatel_oem7_msgs::msg::BESTPOS>(
        "/novatel_bottom/bestpos", 1, std::bind(&GracefulStop::receiveBestgnsspos, this, std::placeholders::_1));
    this->subBestvel_ = this->create_subscription<novatel_oem7_msgs::msg::BESTVEL>(
        "/novatel_bottom/bestvel", 1, std::bind(&GracefulStop::receiveBestvel, this, std::placeholders::_1));

    auto long_node = rclcpp::Node::make_shared("LongControlNode");
    this->long_control_client = std::make_shared<rclcpp::SyncParametersClient>(long_node);
    auto kin_node = rclcpp::Node::make_shared("KinControlNode");
    this->kin_control_client = std::make_shared<rclcpp::SyncParametersClient>(kin_node);
} // class



void GracefulStop::receiveStopCmd(const std_msgs::msg::Bool::SharedPtr msg)
{
    this->stop_enable = msg->data;
    if(this->stop_enable == true) { // Graceful Stopping
        this->long_control_client->set_parameters({rclcpp::Parameter("auto_enabled",true)});
        this->long_control_client->set_parameters({rclcpp::Parameter("desired_velocity",0.0)});
    }
}

void GracefulStop::receiveBestgnsspos(const novatel_oem7_msgs::msg::BESTPOS::SharedPtr msg)
{
    auto lat_stdev = msg->lat_stdev;
    auto long_stdev = msg->lon_stdev;
    if (this->checks_enable && this->kin_control_client->get_parameter("auto_enabled",false))
    {
        if (lat_stdev > 0.35 || long_stdev > 0.35) {
            this->long_control_client->set_parameters({rclcpp::Parameter("auto_enabled",true)});
            this->long_control_client->set_parameters({rclcpp::Parameter("desired_velocity",0.0)});
        }
    }
}

void GracefulStop::receiveBestvel(const novatel_oem7_msgs::msg::BESTVEL::SharedPtr msg)
{
    auto ego_vel = msg->hor_speed;
    if (ego_vel > 10.0) // Start checking when 10m/s exceeded once
    {
        this->checks_enable = true;
    }

}
    

} // namespace

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<control::GracefulStop>());
  rclcpp::shutdown();
  return 0;
}

