#include <memory>
#include <deep_orange_msgs/msg/rc_to_ct.hpp>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float32.hpp"
using std::placeholders::_1;
using namespace std::chrono_literals;
class MinimalSubscriber : public rclcpp::Node
{
  public:
    MinimalSubscriber()
    : Node("subscriber")
    {
      subscription_ = this->create_subscription<std_msgs::msg::Float32>(
      "velocity_input", 10, std::bind(&MinimalSubscriber::topic_callback, this, _1));
      publisher_ = this->create_publisher<std_msgs::msg::Float32>("velocity", 10);
      timer_ = this->create_wall_timer(
      500ms, std::bind(&MinimalSubscriber::timer_callback, this));
      //For RC flag input
      rc_subscriber = this->create_subscription<deep_orange_msgs::msg::RcToCt>(
      "rc_flag", 10, std::bind(&MinimalSubscriber::rc_callback, this, _1));
      rc_publisher_ = this->create_publisher<deep_orange_msgs::msg::RcToCt>("raptor_dbw_interface/rc_to_ct", 10);
      rc_timer_ = this->create_wall_timer(
      500ms, std::bind(&MinimalSubscriber::rc_timer_callback, this));
      
    }

  private:
    float v = 0.00;
    unsigned int rcflag = 1;
    std::array<bool, 16> black = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
    std::array<bool, 16> checkered = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
    std::array<bool, 16> purple = {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
    
    void topic_callback(const std_msgs::msg::Float32::SharedPtr msg) 
    {
      RCLCPP_INFO(this->get_logger(), "I heard: '%f'", msg->data);
      v = msg->data;
    }
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr subscription_;

    void timer_callback()
    {
      auto velocity = std_msgs::msg::Float32();
      velocity.data = v;
      // RCLCPP_INFO(this->get_logger(), "Publishing: '%f'", v);
      publisher_->publish(velocity);
    }
    rclcpp::TimerBase::SharedPtr timer_;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr publisher_;

    //For RC flag
    void rc_callback(const deep_orange_msgs::msg::RcToCt::SharedPtr msg) 
    {
      RCLCPP_INFO(this->get_logger(), "Track Condition: '%u'", msg->track_cond);
      rcflag = msg->track_cond;
      black  = msg->black;
      checkered = msg->checkered;
      purple = msg->purple;
    }
    rclcpp::Subscription<deep_orange_msgs::msg::RcToCt>::SharedPtr rc_subscriber;
    void rc_timer_callback()
    {
      auto RCInfo = deep_orange_msgs::msg::RcToCt();
      RCInfo.track_cond = rcflag;
      RCInfo.black = black;
      RCInfo.checkered = checkered;
      RCInfo.purple = purple;

      // RCLCPP_INFO(this->get_logger(), "Publishing: '%u'", rcflag);
      // rc_publisher_->publish(RCInfo);
    }
    rclcpp::TimerBase::SharedPtr rc_timer_;
    rclcpp::Publisher<deep_orange_msgs::msg::RcToCt>::SharedPtr rc_publisher_;
};

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalSubscriber>());
  rclcpp::shutdown();
  return 0;
}